#+TITLE: Emacs configuration file
#+AUTHOR: Craig Eales
#+BABEL: :cache yes
#+PROPERTY: header-args :tangle yes

* About

* Configuration

** Meta

   The system is boot strapped by the following init.el file. It
   simply looks to see if ce-init.el is missing or if ce-init.org is
   *newer* than it. If this is the case then it loads ce-init.org and
   tangles it to generate a new ce-init.el.

   This code used to also then
   compule ce-init.el to ce-init.elc for faster loading next time.
   However, this seemed to be quite fragile so I live with the longer
   loading times.

   Finally ce-init.el is loaded.

#+BEGIN_SRC emacs-lisp :tangle no :file init.el
  ;; This is added to stop the package system adding it anyway
  ;;(package-initialize)

  ;; If the ce-init.org is newer than ce-init.el then tangle
  ;; ce-init.org

  (let ((ce-init-org-file (concat user-emacs-directory "ce-init.org"))
	(ce-init-el-file (concat user-emacs-directory "ce-init.el")))
    (when (or (not (file-exists-p ce-init-el-file))
	      (file-newer-than-file-p ce-init-org-file ce-init-el-file))
      (require 'org)
      (find-file ce-init-org-file)
      (org-babel-tangle)
      (kill-buffer "ce-init.org")))

  ;; Continue loading the tangled el file
  (load-file (concat user-emacs-directory "ce-init.el"))
#+END_SRC

** Core

   Here we set up the primitive emacs machiney for the rest of the
   configuration. We define some mechanics for /site local/
   configuration and then boot-strap the in-built /package/ system of
   emacs.

*** Variables

    We are going to need some /site local/ configuration. Rather than
    letting the local config go free-ride it seems better to orientate
    the local settings around a collection of pre-defined variables.

#+BEGIN_SRC emacs-lisp
  (defvar ce/default-font "Hack 20"
    "The default font use in ce-init.org")

  (defvar ce/emacs-autosave-directory
    (concat user-emacs-directory "autosaves/")
    "This variable dictates where to put auto saves. It is set to a
  directory called autosaves located wherever your .emacs.d/ is
  located.")

  (defvar ce/life-org-path
    "~/org/"
    "This variable holds the path to where my calendar, tasks, etc
  org files are held")

  (defvar ce/start-emacs-server
    t
    "Do we start the emacs server")
#+END_SRC

*** Private Settings

    This is where the /site local/ variables defined in [[*Variables][Variables]] are
    overriden.

    We check is ce-private.el exists, if it does then load it before
    continuing.

#+BEGIN_SRC emacs-lisp
  (let ((private-file (concat user-emacs-directory "ce-private.el")))
    (when (file-exists-p private-file)
      (load-file private-file)))
#+END_SRC

*** Emacs Lisp

    Load in the /Common Lisp/ macros and functions to make elisp a
    little more functional.

#+BEGIN_SRC emacs-lisp
  (require 'cl)
#+END_SRC

*** Package Initialisation

    Now we initialise the core package system that is used to load the
    required external packages.

    Firstly a list of archives where external packages are defined. We
    have both melpa and melpa-stable so we can pin more volatile
    packages.

#+BEGIN_SRC emacs-lisp
  (require 'package)
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
  (add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
  (add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
#+END_SRC

    An example of pinning a package to a repository. Here we insist
    the special package org-plus-contrib: a package that contains the
    latest org mode plus the org contrib libararies are installed from
    the org repo.

#+BEGIN_SRC emacs-lisp
  (add-to-list 'package-pinned-packages '(org-plus-contrib . "org") t)
#+END_SRC

    Finally, we are ready to initialise the package system.

#+BEGIN_SRC emacs-lisp
  (package-initialize)
#+END_SRC

    Now we are ready to install any missing packages.

#+BEGIN_SRC emacs-lisp
  (let* ((packages
	  '(bind-key
	    delight
	    use-package
	    magit
	    undo-tree
	    which-key
	    ivy
	    ivy-hydra
	    counsel
	    org-plus-contrib
	    avy
	    ace-window
	    smartparens
	    clojure-mode
	    clj-refactor
	    cider
	    solarized-theme
	    ivy-rich
	    anzu
	    smex
	    projectile
	    company
	    ag
	    cmake-mode
	    rust-mode
	    yaml-mode
	    markdown-mode
	    elscreen
	    zoom-window
	    ggtags
	    pomodoro
	    iedit
	    god-mode
	    origami
	    rainbow-delimiters
	    git-gutter
	    )))
    (ignore-errors
      (let ((packages (remove-if 'package-installed-p packages)))
	(when packages
	  (package-refresh-contents)
	  (mapc 'package-install packages)))))
#+END_SRC

** Basic Configuration

   The core system is now ready, we now add local configuration that
   is not based on packages.

*** OS Specific Settings

    Different OS's and window systems require some local config to
    improve the quality of life.

**** Mac OSX

     On my Mac Book Pro, the location of the modifier keys require
     some re-mapping for comfort.

     I use the /command/ key on the Mac as the /meta/ modifier that is
     normally bound to /Alt/ on a windows keyboard.

     I unbound the /left alt key/ from acting as /meta/ this lets me
     use left-alt to insert the # character.

     Finally, the right-hand side of the keyboard is missing a /ctrl/
     key so I use the /right alt/ as a control key.

#+BEGIN_SRC emacs-lisp
  (when (eq window-system 'ns)
      (setq ns-right-alternate-modifier 'control)
      (setq ns-alternate-modifier 'none)
      (setq ns-command-modifier 'meta))
#+END_SRC

*** Fonts and Faces

    Simply set the standard font to the one specified in the site
    local config.

#+BEGIN_SRC emacs-lisp
  (set-frame-font ce/default-font)
#+END_SRC

*** Theme

    The /Leuven/ theme seems to work well for my colour issues right
    now. This could probably be made a site-local variable.

    I have changed to solarized-dark as I like the dark themes.

#+BEGIN_SRC emacs-lisp
  (load-theme 'solarized-dark t)
#+END_SRC

*** Better Defaults

    Quite a few of the default settings in emacs do not suit my world
    view. So just fiddle with a few things, mainly keeping files in
    one place etc.

**** Set custom to a named file

     The org-file init file set up does not play nicely with the
     default /custom/ system; by default the settings are written into
     the end of the init.el file.

#+BEGIN_SRC emacs-lisp
  (setq custom-file (concat user-emacs-directory "custom.el"))
  (load custom-file)
#+END_SRC

**** Turn off splash screens

     We definitely do not need a splash screen at start up.

#+BEGIN_SRC emacs-lisp
  (setq inhibit-startup-message t
	initial-scratch-message nil)
#+END_SRC

**** Text Settings

     A few text based settings: I do not use the double space for end of
     sentence (maybe I should?), I like auto-fill-mode and I also like the 80
     column width. Finally, I do not like lines that wrap.

#+BEGIN_SRC emacs-lisp
  (setq sentence-end-double-space nil
	fill-column 79
	truncate-lines t)
#+END_SRC

**** Screen Real Estate

     Let's keep the screen for work and not fancy tool bars etc.

#+BEGIN_SRC emacs-lisp
  (scroll-bar-mode -1)
  (tool-bar-mode -1)
  (menu-bar-mode -1)
#+END_SRC

**** Backup files

     Dropping turds into every work directory is anti-social. Let's have a
     central place for backups and auto-saves.

#+BEGIN_SRC emacs-lisp
  (unless (file-exists-p ce/emacs-autosave-directory)
    (make-directory ce/emacs-autosave-directory))

  (setq backup-directory-alist
	`((".*" . ,ce/emacs-autosave-directory))
	auto-save-file-name-transforms
	`((".*" ,ce/emacs-autosave-directory t)))
#+END_SRC

**** Yes Or No

     I am happy that /y/ or /n/ are enough to confirm a choice. I do not need
     the full /yes/ /no/ typing.

#+BEGIN_SRC emacs-lisp
  (fset 'yes-or-no-p 'y-or-n-p)
#+END_SRC

**** UTF-8 Mode

     We are in the modern world, UTF-8 seems a good choice for a default.

#+BEGIN_SRC emacs-lisp
  (set-language-environment "UTF-8")
#+END_SRC

**** Enable Narrow to Region

     Narrowing is a good thing, and I know the key combo /C-x n w/ to undo any
     narrowing I have got myself into.

#+BEGIN_SRC emacs-lisp
  (put 'narrow-to-region 'disabled nil)
#+END_SRC


**** Transient Mark Mode

     Let's try turning this off, I like narrowing.

     #+BEGIN_SRC emacs-lisp
       (transient-mark-mode 0)
     #+END_SRC

*** Emacs Server

    Do we start the server? With it enabled we can use emacsclient as our
    $EDITOR

    #+BEGIN_SRC emacs-lisp
      (when ce/start-emacs-server
	(server-start)
	(message "Emacs Server Started"))
    #+END_SRC

*** Focus Follows Mouse

    Have emacs behave like twm and friends.

    #+BEGIN_SRC emacs-lisp
      (setq mouse-autoselect-window t)
    #+END_SRC


*** Tramp

#+begin_src emacs-lisp
  (use-package tramp
    :config (progn
	      (add-to-list 'tramp-remote-path 'tramp-own-remote-path)))
#+end_src

** Packages
   Package initilization and configuration are handled by John Weigley's
   [[https://github.com/jwiegley/use-package][use-package]] system. [[info:use-package#Top][info:use-package]]

*** God Mode

    A poor man's modal editing.

    #+BEGIN_SRC emacs-lisp
      (use-package god-mode
	:bind ("<escape>" . god-mode-all)
	:config (progn (setq god-exempt-major-modes nil)
		       (setq god-exempt-predicates nil)
		       (defun my-update-cursor ()
			 (setq cursor-type (if (or god-local-mode buffer-read-only)
					       'box
					     'bar)))
		       (add-hook 'god-mode-enabled-hook 'my-update-cursor)
		       (add-hook 'god-mode-disabled-hook 'my-update-cursor)
		       (define-key god-local-mode-map (kbd ".") 'repeat)))
    #+END_SRC

*** Source Control

    Install /Magit/ the /git/ porcelein. Bind status to C-c m.
    The manual is in info [[info:magit#Top][info:magit]].

#+BEGIN_SRC emacs-lisp
  (use-package magit
    :bind ("C-c m" . magit-status))
#+END_SRC

#+BEGIN_SRC emacs-lisp
  (use-package git-gutter
    :delight
    :config (progn (global-git-gutter-mode 1)
		   (add-hook 'magit-post-refresh-hook #'git-gutter:update-all-windows)))
#+END_SRC

*** Undo Tree

    The built-in undo mechanism of emacs is powerful but not easy to navigate.
    /Undo Tree/ is an alternative mechanism. Here a tree of undo\/redo state is
    maintained and is available in a visual form.

    The documnetation is [[https://elpa.gnu.org/packages/undo-tree.html][here]].

    The global mode installs its own key bindings that hijack the default undo
    (C-\/) redo (/C-?/) and binds /C-x u/ to the visualizer.

#+BEGIN_SRC emacs-lisp
  (use-package undo-tree
    :delight
    :config (global-undo-tree-mode 1)
	    (setq undo-tree-visualizer-timestamps t)
	    (setq undo-tree-visualizer-diff t))
#+END_SRC

*** Recent Files

    Configure the in-built package recentf. We dont need to re-bind "C-x C-r"
    to it as ivy has a special virtual buffer system, that puts the recent
    files as virtual buffers.

#+BEGIN_SRC emacs-lisp
  (use-package recentf
    :config
    (progn (setq recentf-max-saved-items 50)
	   (setq recentf-exclude '("/.*\\.emacs\\.d/elpa/.*"
				   "/.*\\.emacs\\.d/bookmarks"
				   ".*\.gitignore"))
	   (recentf-mode 1))
    )
#+END_SRC

*** Avy + Ace Window
#+BEGIN_SRC emacs-lisp
  (use-package avy
    :config (progn
	      (setq avy-all-windows nil)
	      (setq avy-styles-alist '((avy-goto-line . pre)
				       (avy-kill-region . pre)
				       (avy-goto-char-timer . at)
				       (avy-goto-char-in-line . at)
				       (avy-goto-word-or-subword-1 . at)
				       ))
	      )
    :bind (("C-c C-j" . avy-resume)
	   ("C-c C-SPC C-l" . avy-goto-line)
	   ("C-c C-SPC C-w" . avy-goto-char-timer)
	   ("C-c C-SPC C-c" . avy-goto-char-in-line)
	   ("C-c C-SPC C-s" . avy-goto-word-or-subword-1)
	   ("C-c C-SPC C-k" . avy-kill-region)
	   )
    )
#+END_SRC

#+BEGIN_SRC emacs-lisp
  (use-package ace-window
    :config (progn (setq aw-dispatch-always t))
    :bind (("C-c C-SPC C-SPC" . ace-window))
    )
#+END_SRC

*** Org
    By default imenu only lists 2 levels of depth, increase this to 3.

    The key bindings are those suggested in the [[info:org#Top][manual]].

#+BEGIN_SRC emacs-lisp
  (use-package org
    :config (setq org-imenu-depth 3)
    :bind (("C-c l" . org-store-link)
	   ("C-c a" . org-agenda)
	   ("C-c c" . org-capture)
	   ("C-c b" . org-switchb))
    :config
    (setq org-directory ce/life-org-path)
    (setq org-default-notes-file (concat ce/life-org-path "capture.org"))
    (setq org-agenda-files (list (concat ce/life-org-path "tasks.org")
				 (concat ce/life-org-path "calendar.org")))
    (setq org-todo-keywords
	  '((sequence "TODO(t!)" "NEXT(n!)" "WAITING(w@/@)"
		      "|" "DONE(d)" "DELEGATED(e@)" "CANCELLED(c@)" "DEFERRED(D@)")))
    (setq org-refile-targets  (list '(nil :maxlevel . 3)
				    (cons (concat ce/life-org-path "tasks.org") '(:maxlevel . 2))
				    (cons (concat ce/life-org-path "calendar.org") '(:maxlevel . 2))))
    )

    (defun ce/org-ws-mode-hook ()
    (set (make-local-variable 'whitespace-style) '(face trailing empty))
    (whitespace-mode 1))


    (add-hook 'org-mode-hook 'ce/org-ws-mode-hook)
#+END_SRC

*** Ivy + Counsel + Swiper + SMEX

    Ivy is a generic completion mechanism. It infilitrates many points
    where items can be selected from a list e.g. find-file,
    select-buffer.

    The manual can be found [[http://oremacs.com/swiper/][here]]

    Ivy is the general completion mechanism.
    Counsel is a set of extra commands that use ivy to present lists.
    Swiper is an isearch replacement that uses the ivy mechanism

    M-o is very useful as it opens the hydra which exposes actions
    Double tab open a directory in the file finder

    SMEX is an enhancement to M-x, ivy uses it by default if available. It
    gives quick access to previous commands.


 #+BEGIN_SRC emacs-lisp
   (use-package ivy
     :demand
     :delight ivy-mode
     :delight counsel-mode
     :config
     (defun ivy-jump-to-window ()
       (interactive)
       (let* (
	      (visible-buffers-assoc (mapcar (lambda (window) (cons (buffer-name (window-buffer window)) window)) (window-list)))
	      (visible-buffers (delete-dups (mapcar 'car visible-buffers-assoc)))
	      (buffer-name (ivy-completing-read "Window Buffer:" visible-buffers nil 't))
	      )
	 (if buffer-name
	     (select-window (cdr (assoc buffer-name visible-buffers-assoc))))
	 nil
	 )
       )
     (ivy-mode 1)
     (counsel-mode 1)
     (ivy-rich-mode 1)
     (setq ivy-use-virtual-buffers t)
     (setq ivy-count-format "(%d/%d) ")

     (setq magit-completing-read-function 'ivy-completing-read)
     (setq projectile-completion-system 'ivy)
     :bind
     (("C-s" . swiper)
      ("<f6>" . ivy-resume)
      ("C-c w" . ivy-jump-to-window)
      ("C-x C-r" . counsel-recentf))
     )
 #+END_SRC

 #+BEGIN_SRC emacs-lisp
   (use-package smex)
 #+END_SRC

*** IMenu
    IMenu is built-in to emacs, but it is not bound to a sensible key.
    M-i is tab-to-tab-stop and not really needed so rebind it to
    imenu.

#+BEGIN_SRC emacs-lisp
  (use-package imenu
    :bind (("C-c i" . imenu)))
#+END_SRC

*** Which Key
    [[https://github.com/justbur/emacs-which-key][Which-Key]] is a package to display a menu of key bindings that are
    available after a prefix.

    If the list of options is too many to display on the screen then
    the default key binding to scroll through the list is "C-h".

#+BEGIN_SRC emacs-lisp
  (use-package which-key
    :delight
    :config (progn
	      (which-key-mode 1)
	      (setq ivy-use-virtual-buffers t)
	      (setq ivy-count-format "(%d/%d)")
	      (which-key-add-key-based-replacements
		"C-x 8" '("unicode" . "Unicode keys")
		"C-x 4" '("other win" . "Other Window")
		"C-x 5" '("other frame" . "Other Frame")
		"C-x n" '("narrow" . "Narrowing")
		"C-x r" '("registers" . "Registers")
		"C-c C-SPC" '("avy" . "Avy Jump")
		)
	      )
    )
#+END_SRC

*** Smart Parens
 #+BEGIN_SRC emacs-lisp
   (defun ce/sp-standard-hook ()
     (smartparens-strict-mode 1)
     (show-smartparens-mode 1))

   (use-package smartparens
     :demand
     :config (progn
	       (require 'smartparens-config)
	       )
     :bind (:map smartparens-mode-map
		 ("C-M-f" . 'sp-forward-sexp)
		 ("C-M-b" . 'sp-backward-sexp)

		 ("C-M-d" . 'sp-down-sexp)
		 ("C-M-u" . 'sp-up-sexp)

		 ("C-M-a" . 'sp-beginning-of-sexp)
		 ("C-M-e" . 'sp-end-of-sexp)

		 ("C-S-d" . 'sp-backward-down-sexp)
		 ("C-S-u" . 'sp-backward-up-sexp)

		 ("C-M-n" . 'sp-next-sexp)
		 ("C-M-p" . 'sp-previous-sexp)

		 ("C-M-k" . 'sp-kill-sexp)
		 ("C-M-w" . 'sp-copy-sexp)

		 ("C-M-t" . 'sp-transpose-sexp)

		 ("M-<delete>" . 'sp-unwrap-sexp)
		 ("M-<backspace>" . 'sp-backward-unwrap-sexp)

		 ("C-M-<right>" . 'sp-forward-slurp-sexp)
		 ("C-M-<left>" . ' sp-backward-slurp-sexp)
		 ("C-S-<left>" . ' sp-forward-barf-sexp)
		 ("C-S-<right>" . 'sp-backward-barf-sexp)

		 ("C-M-<" . 'sp-select-previous-thing)
		 ("C-M->" . 'sp-select-next-thing)

		 ("M-F" . 'sp-forward-symbol)
		 ("M-B" . 'sp-backward-symbol)

		 ("C-M-\"" . 'sp-change-inner)
		 ("M-i" . 'sp-change-enclosing)))
 #+END_SRC

*** Wind Move

    Use the windmove package, to bind some *easy* bindings to window motion
    commands

    #+BEGIN_SRC emacs-lisp
      (use-package windmove
	:bind (("C-c <left>" . 'windmove-left)
	       ("C-c <right>" . 'windmove-right)
	       ("C-c <up>" . 'windmove-up)
	       ("C-c <down>" . 'windmove-down)))
    #+END_SRC

*** Anzu Search Replace

    Gives a preview of search replace

    #+BEGIN_SRC emacs-lisp
      (use-package anzu
	:delight
	:config (progn (global-anzu-mode)
		       (global-set-key [remap query-replace] 'anzu-query-replace)
		       (global-set-key [remap query-replace-regexp] 'anzu-query-replace-regexp)))
    #+END_SRC

*** Projectile
    Projectile is considered the best "Project" interface for emacs.
    [[https://github.com/bbatsov/projectile]]

    #+BEGIN_SRC emacs-lisp
      (use-package projectile
	:delight
	:config (progn (projectile-mode +1)
		       (setq projectile-enable-caching t)
		       (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)))
    #+END_SRC

*** Company Auto Completion
    Company is an auto completion engine
    [[http://company-mode.github.io/]]

    #+BEGIN_SRC emacs-lisp
      (use-package company
	:delight " CO"
	:config (progn (setq company-idle-delay nil)))
    #+END_SRC

*** File Searching
**** AG Silver Searcher
     Says it all :)
     Required for use with projectile, for search in project with ag.

     #+BEGIN_SRC emacs-lisp
       (use-package ag)
     #+END_SRC
*** GGTags

    GGTags is the interface to Gnu Global, an old school style source code
    tagger, it appears to work well.

    #+BEGIN_SRC emacs-lisp
      (use-package ggtags)
    #+END_SRC

*** Pomodoro

    #+BEGIN_SRC emacs-lisp
      (use-package pomodoro
	:config (progn (setq pomodoro-play-sounds nil)
		       (setq pomodoro-show-number t)
		       (pomodoro-add-to-mode-line)))
    #+END_SRC

*** Origami

    General folding/unfolding

#+begin_src emacs-lisp
  (use-package origami
    :delight
    :config (unbind-key "M-o")
    :bind (:map origami-mode-map
		("M-o M-o" . origami-toggle-node)
		("M-o o" . origami-open-node)
		("M-o C-o" . origami-open-node-recursively)
		("M-o c" . origami-close-node)
		("M-o C-c" . origami-close-node-recursively)
		("M-o r" . origami-reset)
		("M-o SPC" . origami-show-only-node)
		("M-o <" . origami-close-all-nodes)
		("M-o >" . origami-open-all-nodes)
	   )
    )
#+end_src

*** Languages

**** C++

     #+BEGIN_SRC emacs-lisp
       (defun ce/c++-mode-hook ()
	 (company-mode 1)
	 (local-set-key (kbd "<C-tab>") #'company-complete)

	 (set (make-local-variable 'whitespace-line-column) 80)
	 (set (make-local-variable 'whitespace-style) '(face tabs trailing empty indentation::space lines-tail))
	 (whitespace-mode 1)

	 (origami-mode 1)
	 (rainbow-delimiters-mode-enable)

	 (ggtags-mode 1)

	 (setq indent-tabs-mode nil)

	 (c-toggle-hungry-state 1)
	 (electric-indent-local-mode -1))

       (add-to-list 'auto-mode-alist (cons "\\.h\\'" 'c++-mode))

       (add-hook 'c++-mode-hook 'ce/c++-mode-hook)
     #+END_SRC


***** CMake

      #+BEGIN_SRC emacs-lisp
	(use-package cmake-mode
	  :config
	  (progn (defun ce/cmake-mode-hook ()
		   (company-mode 1)
		   (local-set-key (kbd "<C-tab>") #'company-complete)
		   (smartparens-mode 1)
		   (show-smartparens-mode 1))
		 (add-hook 'cmake-mode-hook 'ce/cmake-mode-hook)))
      #+END_SRC

**** Clojure

 #+BEGIN_SRC emacs-lisp
   (use-package clojure-mode)
   (use-package clj-refactor)
   (use-package cider
     :config
     (setq cider-session-name-template "%J:%h:%p:%r"))

   (defun ce/clojure-mode-hook ()
     (clj-refactor-mode 1)
     (cljr-add-keybindings-with-prefix "C-c C-m")
     (rainbow-delimiters-mode-enable)
     )

   (add-hook 'clojure-mode-hook 'ce/clojure-mode-hook)

   (defun ce/cider-mode-hook ()
     (company-mode 1)
     (local-set-key (kbd "<C-tab>") #'company-complete))

   (add-hook 'cider-repl-mode-hook #'ce/cider-mode-hook)
   (add-hook 'cider-mode-hook #'ce/cider-mode-hook)

   (add-hook 'clojure-mode-hook 'ce/sp-standard-hook)

   (defun ce/clojure-ws-mode-hook ()
     (set (make-local-variable 'whitespace-line-column) 80)
     (set (make-local-variable 'whitespace-style) '(face tabs trailing empty indentation::space lines-tail))
     (whitespace-mode 1))

   (add-hook 'clojure-mode-hook 'ce/clojure-ws-mode-hook)
 #+END_SRC

**** ELisp

     #+BEGIN_SRC emacs-lisp
       (defun ce/emacs-lisp-mode-hook ()
	 (company-mode 1)
	 (local-set-key (kbd "<C-tab>") #'company-complete)
	 (ce/sp-standard-hook)
	 (rainbow-delimiters-mode-enable)
	 (set (make-local-variable 'whitespace-line-column) 80)
	 (set (make-local-variable 'whitespace-style) '(face trailing empty lines-tail))
	 (whitespace-mode 1)
	 )

       (add-hook 'emacs-lisp-mode-hook 'ce/emacs-lisp-mode-hook)
     #+END_SRC


**** Markdown

     #+BEGIN_SRC emacs-lisp
       (use-package markdown-mode
	 :commands (markdown-mode gfm-mode)
	 :mode (("README\\.md\\'" . gfm-mode)
		("\\.md\\'" . markdown-mode)
		("\\.markdown\\'" . markdown-mode))
	 :init (setq markdown-command "multimarkdown"))
     #+END_SRC

*** ElScreen + Zoom Window

    Elscreen gives screen/tmux like functionality in emacs, sounds perfect for
    my text mode of working in a GUI environment. It is on the useless prefix
    of C-z a much better use of a prime key! Documentation [[https://github.com/knu/elscreen][here]].

    Zoom Window, adds the ability to zoom in a window of a screen. The
    repository is [[https://github.com/syohex/emacs-zoom-window][here]].

    #+BEGIN_SRC emacs-lisp
      (use-package elscreen
	:config (progn
		  (setq elscreen-display-tab nil)
		  (elscreen-start)))

      (use-package zoom-window
	:config (progn
		  (setq zoom-window-use-elscreen t)
		  (setq zoom-window-mode-line-color "DarkGreen")
		  (zoom-window-setup))
	:bind ("C-z C-z" . zoom-window-zoom))
    #+END_SRC

*** IEdit

#+BEGIN_SRC emacs-lisp
  (use-package iedit)
#+END_SRC


** Code

*** Beginning of line

    Change the behaviour of C-a, instead of just jumping to column 0, jump to
    start of indentation, unless we are already there in which case go to
    column 0.

#+BEGIN_SRC emacs-lisp
  (defun ce/start-of-line ()
    "Move to the start of the text and if already there then move to the start of the line"
    (interactive)
    (let ((x (point)))
      (back-to-indentation)
      (when (eq x (point))
	  (move-beginning-of-line nil))))

  (global-set-key (kbd "C-a") 'ce/start-of-line)
#+END_SRC

*** Open line below & above

    How are these not builtin?

#+BEGIN_SRC emacs-lisp
  (defun ce/open-line-below ()
    "Open a new line below this one and indent if applicable"
    (interactive)
    (move-end-of-line 1)
    (newline 1 t))

  (defun ce/open-line-above ()
    "Open a new line above this one and indent if applicable"
    (interactive)
    (previous-line)
    (ce/open-line-below))

  (global-set-key (kbd "C-<return>") 'ce/open-line-below)
  (global-set-key (kbd "M-<return>") 'ce/open-line-above)
#+END_SRC

*** Buffer File Name Copy

    #+BEGIN_SRC emacs-lisp
      (defun ce/save-buffer-file-name ()
	(interactive)
	(kill-new buffer-file-name))

      (global-set-key (kbd "C-c k b") 'ce/save-buffer-file-name)
    #+END_SRC

* Trailer
# Local Variables:
# fill-column: 79
# eval: (auto-fill-mode 1)
# End:

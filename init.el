;; This is added to stop the package system adding it anyway
;;(package-initialize)

;; If the ce-init.org is newer than ce-init.el then tangle
;; ce-init.org

(let ((ce-init-org-file (concat user-emacs-directory "ce-init.org"))
      (ce-init-el-file (concat user-emacs-directory "ce-init.el")))
  (when (or (not (file-exists-p ce-init-el-file))
	    (file-newer-than-file-p ce-init-org-file ce-init-el-file))
    (require 'org)
    (find-file ce-init-org-file)
    (org-babel-tangle)
    (kill-buffer "ce-init.org")))

;; Continue loading the tangled el file
(load-file (concat user-emacs-directory "ce-init.el"))
